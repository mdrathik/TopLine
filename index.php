<?php
$abc='pdf/abc.pdf';
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Toopline Baladesh</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/heroic-features.css" rel="stylesheet">
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="//rawgithub.com/ashleydw/lightbox/master/dist/ekko-lightbox.js"></script>-->

    <script src="js/custom.js"></script>
    <![endif]-->
    <style>


        .topspace{
            margin:100px auto;max-width:805px;
        }
    </style>

</head>

<body>


<?php require 'navbar.php'; ?>



    <div class="container div_body">

   <!--      Jumbotron Header -->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="img/back.jpg" alt="First slide">
                    <!-- Static Header -->
                    <div class="header-text hidden-xs">
                        <div class="col-md-12 text-center">
                            <h2>
                                <span>Welcome to our <strong>Topline Bangladesh</strong></span>
                            </h2>
                            <br>
                            <h3>
                                <span>Be Client Oriented</span>||<span>Be Professional</span>
                            </h3>
                            <h3>
                                <span>Be Knowledgeable</span>||<span>Be Knowledgeable</span>
                            </h3>
                            <br>
                            <div class="">
                               </div>
                        </div>
                    </div><!-- /header-text -->
                </div>
                <div class="item">
                    <img src="img/back.jpg" alt="Second slide">
                    <!-- Static Header -->
                    <div class="header-text hidden-xs">
                        <div class="col-md-12 text-center">
                            <h2>
                                <span>Welcome to our <strong>Topline Bangladesh</strong></span>
                            </h2>
                            <br>
                            <h3>
                                <span>Be Serve the Quality</span>||<span>Be Competitive</span>
                            </h3>
                            <h3>
                                <span>Be On Time</span>||<span>Be On Time</span>
                            </h3>
                            <br>
                            <div class="">
                            </div>
                        </div>
                    </div><!-- /header-text -->
                </div>
            </div>
        </div><!-- /carousel -->
        <hr>

        <!-- Title -->
        <div class="row">

            <div  class="col-lg-12">

            <h2 class="text-center" style="font-size: 80px;font-family: 'Brush Script MT';color: #5cb85c;">Our Topline</h2>
  <p >TOPLINE starts it’s journey back in the year 1999. TOPLINE is an establish
business firm dealing with military hardware and all defense items for the

Armed forces and also allied organizations of the People Republic of Bangladesh.

The firm is duly registered in the Directorate General Defense Purchase (DGDP),

Ministry of Defense Government of Bangladesh.</p>


<p>The Chief Executive and the Proprietor, Dr. Mahfuz Shafique, of the Firm

is a young successful businessman. He Completed M.B.B.S., Bangladesh Medical

College (DU) and M.Sc. (Health Economics), Dhaka University.</p>

<hr>
<h2 class="text-center" style="font-size: 40px;font-family: 'Serif';color: #5cb85c;">TOPLINE INTRODUCTION SUPPLY AND CONSTRUCTION</h2>

<p>A Team of 28 persons, Including 11 Engineers are there in Topline for

various supply and Construction works.

Topline has all the Major Bangladesh Government Organization

Enlistments Namely : Roads and Highways, Education Engineering Department

(EED), Civil Aviation Authority of Bangladesh (CAAB) (Civil and Electrical) , Local

Government Engineering Department (LGED), Health Engineering Department

(HED), Dhaka WASA, BIWTA, Bangladesh Agricultural Research Institute (BARI)

Gazipur, Public Works Department (PWD) ETC.</p>

<p>Topline also has all the licenses for the Supply, Import and Export Business

too. Namely : Indenting license, Import license, export license. And Topline is also

a member of Dhaka Chamber of Commerce and Industry (DCCI).</p>

<p>For the construction job done Topline also has the ABC (Electrical license)

too. And all the necessary equipment and Instruments needed for the

Construction works.</p>

<hr>

                <h2 class="text-center" style="font-size: 40px;font-family: 'Serif';color: #5cb85c;">TOPLINE INTRODUCTION DEFENCE</h2>
    
<p>TOPLINE has been formed by a group of individuals’ advisors who have

earned distinction in their respective fields. One of advisor of the company

Colonel Chandra Kanta Das (Retired) was an Ordnance Officer of Bangladesh

Army. He served in DGDP as assistant Director for 04 (Four) years and as a Chief

Inspector for 03 (Three) years in the inspection department of Defense Stores. He

is a qualified Ammunition &amp; Explosive Expert (qualified in Defense course in

foreign country). Besides he served in all allied Organizations connected with the

procurement of Defense Stores. He maintains close link and association with all

senior and junior officers of Defense and other related Defense establishments in

order to promote business.</p>


<p>One of our advisors Captain AZM Shameem Khan Pathan (L) PSC BN
(Retd.) is a specialist in Electrical and Electronic Engineering in Naval sector. He

has some professional training in Weapon Engineering Application Course in the

United Kingdom, International Defence Management Course in USA and

Operation and Maintenance Course on NA-18 Optronic Fire Control System in

Italy. He was the Director Engineering for 05 (Five) years of Coast Guard

Headquarters of the Bangladesh Navy.</p>


<p>Mr. Khasmat Uz Zamman and Dr. Abul Khosseyn are foreign partner of

our company have been residing in Moscow permanently; they are in best

position to promote our business in Russia and CIS.</p>




<p>With the professionals at the helm of its affairs, TOPLINE contemplates

and intends to pursue its goal in classic manner. In the meantime, it has

established strong link with Defense establishment and as well as civil

administration. It has already secured agencies of a number of international

known suppliers/ manufacturers of defense equipments. TOPLINE has also

permission to deal on foreign procurement.</p>


<p>Topline besides Directorate General of Defense Purchase (DGDP)

Enlistment has all the Major Bangladesh Government Defense Organizations

Enlistments Namely : 201 MU (Bangladesh Air Force), Bangladesh Coast Guard,

Bangladesh Police, CMTD, COD (Army), NSSD, ETC.</p>

            </div>

<hr>






            <div class="col-md-12">
                <hr>
                <div class="col-lg-7 slider_img">
                    <div class="tech-slideshow">
                        <div class="mover-1"></div>
                        <div class="mover-2"></div>
                    </div>
                </div>
                <div class="col-md-5">
                    <h2 style="font-size: 60px;font-family: 'Brush Script MT';color: #5cb85c;" class="text-center">Our Achievements</h2>
                    <p>Lorem dolor sit amet, consectetur adipisiciobis adipisicing elit. Ipsa, ipsam, eligendi, in quo sunt possimus non incidunt odit vero aliquid similique quaerat nam nobis illo aspernatur vitae fugiat numquam repellat.</p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisiciobis adipisicing elit. Ipsa, ipsam, eligendi, in quo sunt possimus non incidunt odit vero aliquid similique quaerat nam nobis illo aspernatur vitae fugiat numquam repellat.</p>
                </div>

            </div>



            <div class="col-md-12 speech">
                <div class="col-lg-6">
                    <h2 style="font-size: 80px;font-family: 'Brush Script MT';color: #5cb85c;" class="text-center">We share our</h2>

                    <h3>Speech Text Here</h3>
                    <p>Lorem i, ipuid similique quaerat nam nobis illo aspernatur vitae fugiat numquam repellat.</p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisiciobis adipisicing elit. Ipsa, ipsam, eligendi, in quo sunt possimus non incidunt odit vero aliquid similique quaerat nam nobis illo aspernatur vitae fugiat numquam repellat.</p>


                    <h3>Speech Text Here</h3>
                    <p>Lorelisnobis illo aspernatur vitae fugiat numquam repellat.</p>
                    <h3>Our Achievements</h3>
                    <p>Loresam, eligendi, in quo sunt possimus non incidunt odit vero aliquid similique quaerat nam nobis illo aspernatur vitae fugiat numquam repellat.</p>

                    <p>eligendi, in quo sunt possimus non incidunt odit vero aliquid similique quaerat nam nobis illo aspernatur vitae fugiat numquam repellat.</p>


                </div>
                <div class="col-lg-6">
                    <img src="img/mahfuz.png" class="img-responsive">
                </div>
            </div>



            <div class="col-md-12 parallax">
                    <h1>Be Aggressive</h1>
                <h1>Be Successful</h1>

            </div>
               </div>                     


 



        <div class="col-md-12">
            <h1 class="htext">Our Regular Clients</h1>
            <div id="footer">
                <div class="container">
                    <div class="row">
                        <br>

                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img height="300px" width="200px"  src="img/partner/cvl.png" alt="">
                                <div class="caption">
                                    <h4>Civil Aviation Authority of Bangladesh</h4>
                                     </div>
                            </div>
                        </div>

                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img height="300px" width="200px"    src="img/partner/dcc..png">
                                <div class="caption">
                                    <h4>Dhaka City Corporation</h4>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img  height="300px" width="200px"   src="img/partner/agree.png">
                                <div class="caption">
                                    <h4>Department of Agricultural Extension</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img  height="300px" width="200px"   src="img/partner/navy.png">
                                <div class="caption">
                                    <h4>Bangladesh Navy (NSSD)</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img height="300px" width="200px"    src="img/partner/wasa.png">
                                <div class="caption">
                                    <h4>Dhaka WASA</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img height="300px" width="200px"    src="img/partner/fp.png">
                                <div class="caption">
                                    <h4>Family Planning Association of Bangladesh</h4>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img  height="300px" width="200px"   src="img/partner/printing.png">
                                <div class="caption">
                                    <h4>Bangladesh Government Printing press, Tejgaon, Dhaka</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img  height="300px" width="200px"   src="img/partner/sprinti.png">
                                <div class="caption">
                                    <h4>Security Printing press , Gazipur</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img  height="300px" width="200px"   src="img/partner/bb.png">
                                <div class="caption">
                                    <h4>Bangladesh Bank</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img  height="300px" width="200px"   src="img/partner/dgdp.png">
                                <div class="caption">
                                    <h4>DGDP</h4>
                                </div>
                            </div>
                        </div>
                            <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img  height="300px" width="200px"   src="img/partner/bcc.png">
                                <div class="caption">
                                    <h4>Bangladesh Cricket Board (BCB)</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img  height="300px" width="200px"   src="img/partner/jute.jpg">
                                <div class="caption">
                                    <h4>Bangladesh Jute Research Institute</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img  height="300px" width="200px"   src="img/partner/eed.jpg">
                                <div class="caption">
                                    <h4>Education Engineering Department (EED)</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img  height="300px" width="200px"   src="img/partner/govt.png">
                                <div class="caption">
                                    <h4>Local Government Engineering Department (LGED)</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img  height="300px" width="200px"   src="img/partner/hel.jpg">
                                <div class="caption">
                                    <h4>Local Government Engineering Department (LGED)e</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img  height="300px" width="200px"   src="img/partner/stoc.png">
                                <div class="caption">
                                    <h4>Department of Live Stock Services, Khamarbari, Farmgate, Dhaka</h4>
                                </div>
                            </div>
                        </div>




                    </div>

                </div>
            </div>


        </div>
<div class="col-md-12">
            <h1 class="htext">Our Banking Partner</h1>
            <div id="footer">
                <div class="container">
                    <div class="row">
                        <br>

                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img height="300px" width="200px"  src="img/banking/Janata_Bank.png" alt="">
                                <div class="caption">
                                    <h4>JANATA BANK LIMITED</h4>
                                     </div>
                            </div>
                        </div>

                         <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img height="300px" width="200px"  src="img/banking/megh.jpg" alt="">
                                <div class="caption">
                                    <h4>MEGHNA BANK LIMITED</h4>
                                     </div>
                            </div>
                        </div>

                        <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img height="300px" width="200px"  src="img/banking/islami.png" alt="">
                                <div class="caption">
                                    <h4>FIRST SECURTIY ISLAMIC BANK LIMITED,</h4>
                                     </div>
                            </div>
                        </div>

                         <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img height="300px" width="200px"  src="img/banking/prime.png" alt="">
                                <div class="caption">
                                    <h4>PRIME BANK LIMITED,</h4>
                                     </div>
                            </div>
                        </div>

                         <div class="col-xs-16 col-sm-4 col-md-2">
                            <div class="thumbnail">
                                <img height="300px" width="200px"  src="img/banking/brack.png" alt="">
                                <div class="caption">
                                    <h4>BRAC BANK LIMITED,</h4>
                                     </div>
                            </div>
                        </div>


</div>
</div>
</div>
</div>



        </div>
        <!-- /.row -->








        <div class="footer-bottom">

            <div class="container-fluid">

                <div class="row">

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                        <div class="copyright">

                            © 2017, All rights reserved

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                        <div class="design">

                            <a href="index.php">ToplineBangladesh </a> |  <a target="_blank" href="http://BD-SERVERS.net">Web Design & Development by BD-SERVERS</a>

                        </div>

                    </div>

                </div>

            </div>

        </div>
        </div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
